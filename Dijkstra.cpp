#include "Dijkstra.hpp"

void Dijkstra(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd) {
    t_graph *current = *root;
    subDijkstra(current->way, &(current->up), opnd, desk, 0);
    subDijkstra(current->way, &(current->down), opnd, desk, 1);
    subDijkstra(current->way, &(current->left), opnd, desk, 2);
    subDijkstra(current->way, &(current->right), opnd, desk, 3);
}

void subDijkstra(int root_dstnc, t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type) {
    if ((*leaf) != NULL) {
        if ((*leaf)->visited != true) {
            (*leaf)->visited = true;
            if ((*leaf)->way == numeric_limits<int>::max())
                opnd->push_back((*leaf));
            (*leaf)->parent = type;
            if ((*leaf)->way > root_dstnc + 1)
                (*leaf)->way = root_dstnc + 1;
            if (((*desk)[(*leaf)->i][(*leaf)->j] != 'S') &&
                ((*desk)[(*leaf)->i][(*leaf)->j] != 'E'))
                (*desk)[(*leaf)->i][(*leaf)->j] = '#';
        }
        refresh(desk);
    }
}

void mainDijkstra(t_graph **start_node, vector<string> *desk) {
    t_graph *current = *start_node;
    current->way = 0;
    vector<t_graph *> opnd;
    opnd.push_back(current);
    while (opnd.empty() != true) {
        current = min(&opnd);
        if (current->end == true) {
            findPath(current, desk);
            return;
        }
        Dijkstra(&current, desk, &opnd);
    }
}