#include "BFS.hpp"

void BFS(t_graph **root, vector<string> *desk, queue<t_graph *> *opnd) {
    t_graph *current = *root;
    subBFS(&(current->up), opnd, desk, 0);
    subBFS(&(current->down), opnd, desk, 1);
    subBFS(&(current->left), opnd, desk, 2);
    subBFS(&(current->right), opnd, desk, 3);
}

void subBFS(t_graph **leaf, queue<t_graph *> *opnd, vector<string> *desk, int type) {
    if ((*leaf) != NULL) {
        if ((*leaf)->visited != true) {
            (*leaf)->visited = true;
            opnd->push((*leaf));
            (*leaf)->parent = type;
            if (((*desk)[(*leaf)->i][(*leaf)->j] != 'S')
                && ((*desk)[(*leaf)->i][(*leaf)->j] != 'E'))
                (*desk)[(*leaf)->i][(*leaf)->j] = '#';
        }
    }
    refresh(desk);
}

void mainBFS(t_graph **start_node, vector<string> *desk) {
    t_graph *current = *start_node;
    current->visited = true;
    queue<t_graph *> opnd;
    opnd.push(current);
    while (opnd.empty() != true) {
        current = opnd.front();
        opnd.pop();
        if (current->end == true) {
            findPath(current, desk);
            return;
        }
        BFS(&current, desk, &opnd);
    }
}
