#pragma once

#include <ctime>
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <deque>
#include <cstdlib>
#include <limits>
#include <stack>

using namespace std;

struct t_graph {
    t_graph *up, *down, *left, *right;
    bool visited, closed, end;
    int i, j, parent, way, dstnc;
};

//void myQsort(int left, int right, vector<t_graph*> *array);
//void mySwap(t_graph **a, t_graph **b);
void refresh(vector<string> *desk);

void findPath(t_graph *end, vector<string> *desk);

t_graph *min(vector<t_graph *> *vect);

int distance(int i1, int i2, int j1, int j2);
