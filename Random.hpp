#pragma once

#include "graph.hpp"

void Random(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd);

void mainRandom(t_graph **start_node, vector<string> *desk);

void subRandom(t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type);
