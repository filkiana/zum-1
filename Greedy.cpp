#include "Greedy.hpp"

void Greedy(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd, int end_i, int end_j) {
    t_graph *current = *root;
    subGreedy(&(current->up), opnd, desk, 0, end_i, end_j);
    subGreedy(&(current->down), opnd, desk, 1, end_i, end_j);
    subGreedy(&(current->left), opnd, desk, 2, end_i, end_j);
    subGreedy(&(current->right), opnd, desk, 3, end_i, end_j);
}

void mainGreedy(t_graph **start_node, vector<string> *desk, int end_i, int end_j) {
    t_graph *current = *start_node;
    current->way = 0;
    vector<t_graph *> opnd;
    opnd.push_back(current);
    while (opnd.empty() != true) {
        current = min(&opnd);
        if (current->end == true) {
            findPath(current, desk);
            return;
        }
        Greedy(&current, desk, &opnd, end_i, end_j);
    }
}

void subGreedy(t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type, int end_i, int end_j) {
    if ((*leaf) != NULL) {
        if ((*leaf)->visited != true) {
            (*leaf)->visited = true;
            if ((*leaf)->way == numeric_limits<int>::max())
                opnd->push_back((*leaf));
            (*leaf)->parent = type;
            if ((*leaf)->way >
                (((*leaf)->i - end_i) * ((*leaf)->i - end_i) + ((*leaf)->j - end_j) * ((*leaf)->j - end_j)))
                (*leaf)->way =
                        ((*leaf)->i - end_i) * ((*leaf)->i - end_i) + ((*leaf)->j - end_j) * ((*leaf)->j - end_j);
            if (((*desk)[(*leaf)->i][(*leaf)->j] != 'S')
                && ((*desk)[(*leaf)->i][(*leaf)->j] != 'E'))
                (*desk)[(*leaf)->i][(*leaf)->j] = '#';
        }
    }
    refresh(desk);
}
