#pragma once

#include "graph.hpp"

void Dijkstra(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd);

void mainDijkstra(t_graph **start_node, vector<string> *desk);

void subDijkstra(int root_dstnc, t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type);
