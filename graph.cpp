#include "graph.hpp"
#include "Dijkstra.hpp"
#include "DFS.hpp"
#include "BFS.hpp"
#include "Random.hpp"
#include "Greedy.hpp"
#include "Astar.hpp"

using namespace std;

/*void mySwap(t_graph **a, t_graph **b) {
    t_graph* t = *b;
    *b = *a;
    *a = t;
}

void myQsort(int left, int right, vector<t_graph *> *array) {
    int l, r, mid;
    l = left;
    r = right;
    mid = (r + l) / 2;
    while (l <= r) {
        while ((*array)[l]->dstnc > (*array)[mid]->dstnc) l++;
        while ((*array)[r]->dstnc < (*array)[mid]->dstnc) r--;
        if (l <= r) {
            mySwap(&(*array)[l], &(*array)[r]);
            l++;
            r--;
        }
    }
    if (l < right) myQsort(l, right,array);
    if (r > left) myQsort(left, r, array);
}*/
vector<string> input(int h_size, int v_size) {
    vector<string> desk;
    desk.resize(v_size);
    for (unsigned int i = 0; i < desk.size(); i++)
        desk[i].resize(h_size);
    for (unsigned int i = 0; i < desk.size(); i++) {
        getc(stdin);
        for (unsigned int j = 0; j < desk[i].size(); j++)
            desk[i][j] = getc(stdin);
    }
    return desk;
}

int expanded(vector<string> *desk) {
    int x = 0;
    for (unsigned int i = 0; i < desk->size(); i++) {
        for (unsigned int j = 0; j < (*desk)[i].size(); j++) {
            if ((*desk)[i][j] == '#') x++;
        }
    }
    return x;
}

void findPath(t_graph *end, vector<string> *desk) {
    t_graph *current = end;
    int p = 0;
    int e = expanded(desk);
    while ((*desk)[current->i][current->j] != 'S') {
        if ((*desk)[current->i][current->j] != 'E')
            (*desk)[current->i][current->j] = 'o';
        switch (current->parent) {
            case 0:
                current = current->down;
                break;
            case 1:
                current = current->up;
                break;
            case 2:
                current = current->right;
                break;
            case 3:
                current = current->left;
                break;
        }
        p++;
    }
    refresh(desk);
    cout << "---------------------------------------\n"
            " \x1b[35mS \x1b[39mStart\n \x1b[35mE \x1b[39mEnd\n \x1b[31m# \x1b[39mOpened node\n \x1b[33mo \x1b[39mPath \n X Wall"
            "\n space Fresh node\n---------------------------------------" << endl;
    cout << " Nodes expanded: " << e << endl << " Path length: " << p << endl;
}

void printDesk(vector<string> desk) {
    for (unsigned int i = 0; i < desk.size(); i++) {
        for (unsigned int j = 0; j < desk[i].size(); j++) {
            if ((desk[i][j] == 'S') || (desk[i][j] == 'E'))
                cout << "\x1b[35m" << desk[i][j] << "\x1b[39m";
            else if (desk[i][j] == 'X')
                cout << desk[i][j];
            else if (desk[i][j] == 'o')
                cout << "\x1b[33m" << desk[i][j] << "\x1b[39m";
            else
                cout << "\x1b[31m" << desk[i][j] << "\x1b[39m";
        }
        cout << endl;
    }
}

vector<vector<t_graph> > buildGraph(vector<string> desk, int end_i, int end_j) {
    vector<vector<t_graph> > graph;
    graph.resize(desk.size());
    for (unsigned int i = 0; i < graph.size(); i++)
        graph[i].resize(desk[i].size());
    int k = 0;
    for (unsigned int i = 1; i < desk.size() - 1; i++)
        for (unsigned int j = 1; j < desk[i].size() - 1; j++) {
            {
                graph[i][j].visited = false;
                graph[i][j].closed = false;
                graph[i][j].way = numeric_limits<int>::max();
                graph[i][j].i = i;
                graph[i][j].j = j;
            }
            if (desk[i][j] != 'X') {
                k++;
                if (desk[i + 1][j] != 'X')
                    graph[i][j].down = &graph[i + 1][j];
                if (desk[i][j + 1] != 'X')
                    graph[i][j].right = &graph[i][j + 1];
                if (desk[i - 1][j] != 'X')
                    graph[i][j].up = &graph[i - 1][j];
                if (desk[i][j - 1] != 'X')
                    graph[i][j].left = &graph[i][j - 1];
                // cout << i << " node added" << endl;
            }
        }
    graph[end_i][end_j].end = true;
    cout << k << endl;
    return graph;
}

void refresh(vector<string> *desk) {
    system("clear ");
    printDesk(*desk);
}

t_graph *min(vector<t_graph *> *vect) {
    t_graph *min = (*vect)[0];
    int i_min = 0;
    for (unsigned int i = 0; i < vect->size(); i++) {
        if ((*vect)[i]->way < min->way) {
            min = (*vect)[i];
            min->way = (*vect)[i]->way;
            i_min = i;
        }
    }
    vect->erase(vect->begin() + i_min);
    return min;
}

int distance(int i1, int i2, int j1, int j2) {
    return abs(i1 - i2) + abs(j1 - j2); //((i1 - i2)*(i1 - i2) +(j1 - j2)*(j1 - j2))/10 + 1 ;
}

void search(t_graph root, int search_type, vector<string> *desk, int end_i, int end_j) {
    t_graph *addr_root = &root;
    switch (search_type) {
        case 0:
            mainDFS(&addr_root, desk);
            break;
        case 1:
            mainBFS(&addr_root, desk);
            break;
        case 2:
            mainDijkstra(&addr_root, desk);
            break;
        case 3:
            mainRandom(&addr_root, desk);
            break;
        case 4:
            mainGreedy(&addr_root, desk, end_i, end_j);
            break;
        case 5:
            mainAstar(&addr_root, desk, end_i, end_j);
            break;
    }
}

int main() {
    int h_size, v_size, type, i_start, j_start, i_end, j_end;
    cin >> h_size >> v_size;
    cin >> type;
    vector<string> desk = input(h_size, v_size);
    cin >> j_start >> i_start;
    cin >> j_end >> i_end;
    desk[i_end][j_end] = 'E';
    desk[i_start][j_start] = 'S';
    vector<vector<t_graph> > graph = buildGraph(desk, i_end, j_end);
    search(graph[i_start][j_start], type, &desk, i_end, j_end);
    //refresh(&desk);
    //printDesk(desk);
    return 0;
}