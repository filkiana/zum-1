#pragma once

#include "graph.hpp"

void Greedy(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd, int end_i, int end_j);

void mainGreedy(t_graph **start_node, vector<string> *desk, int end_i, int end_j);

void subGreedy(t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type, int end_i, int end_j);
