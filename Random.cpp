#include "Random.hpp"

void subRandom(t_graph **leaf, vector<t_graph *> *opnd, vector<string> *desk, int type) {
    if ((*leaf) != NULL) {
        if ((*leaf)->visited != true) {
            opnd->push_back((*leaf));
            (*leaf)->parent = type;
            if (((*desk)[(*leaf)->i][(*leaf)->j] != 'S')
                && ((*desk)[(*leaf)->i][(*leaf)->j] != 'E'))
                (*desk)[(*leaf)->i][(*leaf)->j] = '#';
        }
    }
    refresh(desk);

}

void Random(t_graph **root, vector<string> *desk, vector<t_graph *> *opnd) {
    t_graph *current = *root;
    subRandom(&(current->up), opnd, desk, 0);
    subRandom(&(current->down), opnd, desk, 1);
    subRandom(&(current->left), opnd, desk, 2);
    subRandom(&(current->right), opnd, desk, 3);
    current->visited = true;

}

void mainRandom(t_graph **start_node, vector<string> *desk) {
    t_graph *current = *start_node;
    vector<t_graph *> opnd;
    srand(time(NULL));
    opnd.push_back(current);
    int random_i = 0;
    while (opnd.empty() != true) {
        random_i = 0;
        if (opnd.size() > 1)
            random_i = rand() % (opnd.size());
        current = opnd[random_i];
        opnd.erase(opnd.begin() + random_i);
        if (current->end == true) {
            findPath(current, desk);
            return;
        }
        Random(&current, desk, &opnd);
    }
}

