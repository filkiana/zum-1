#pragma once

#include "graph.hpp"

void subDFS(t_graph **leaf, stack<t_graph *> *opnd, vector<string> *desk, int type);

void DFS(t_graph **root, vector<string> *desk, stack<t_graph *> *opnd);

void mainDFS(t_graph **start_node, vector<string> *desk);