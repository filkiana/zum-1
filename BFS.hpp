#pragma once

#include "graph.hpp"

void subBFS(t_graph **leaf, queue<t_graph *> *opnd, vector<string> *desk, int type);

void BFS(t_graph **root, vector<string> *desk, queue<t_graph *> *opnd);

void mainBFS(t_graph **start_node, vector<string> *desk);