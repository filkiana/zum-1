#include "DFS.hpp"

void DFS(t_graph **root, vector<string> *desk, stack<t_graph *> *opnd) {
    t_graph *current = *root;
    subDFS(&(current->up), opnd, desk, 0);
    subDFS(&(current->down), opnd, desk, 1);
    subDFS(&(current->left), opnd, desk, 2);
    subDFS(&(current->right), opnd, desk, 3);
}

void subDFS(t_graph **leaf, stack<t_graph *> *opnd, vector<string> *desk, int type) {
    if ((*leaf) != NULL) {
        if ((*leaf)->visited != true) {
            (*leaf)->visited = true;
            opnd->push((*leaf));
            (*leaf)->parent = type;
            if (((*desk)[(*leaf)->i][(*leaf)->j] != 'S')
                && ((*desk)[(*leaf)->i][(*leaf)->j] != 'E'))
                (*desk)[(*leaf)->i][(*leaf)->j] = '#';
        }
    }
    refresh(desk);
}

void mainDFS(t_graph **start_node, vector<string> *desk) {
    t_graph *current = *start_node;
    current->visited = true;
    stack<t_graph *> opnd;
    opnd.push(current);
    while (opnd.empty() != true) {
        current = opnd.top();
        opnd.pop();
        if (current->end == true) {
            findPath(current, desk);
            return;
        }
        DFS(&current, desk, &opnd);
    }
}
